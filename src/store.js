import {createStore} from 'easy-peasy';

import {application, dashboardView} from './models';

// could move this out to a di/container-ish func to bootstrap services
// with appropriate config/env
import DataService from './services/data-service';
const dataService = new DataService(process.env.REACT_APP_DATA_SERVICE);

export default createStore({
  application: application(),
  dashboardView: dashboardView({dataService}),
});
