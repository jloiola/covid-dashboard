import React from 'react';
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom';

import {Icon} from '@chakra-ui/core';
import {ErrorPage} from 'chakra-ui-ext';

import DashboardView from './views/DashboardView';

const AppRouter = () => {
  return (
    <Router basename={process.env.REACT_APP_MOUNT_PATH}>
      <Switch>
        <Route path="/dashboard" component={DashboardView} />
        <Route path="/forecast" component={DashboardView} />
        <Route path="/performance" component={DashboardView} />
        <Route path="/filters" component={DashboardView} />
        <Route path="/reporting" component={DashboardView} />

        <Route path="/">
          <Redirect to="/dashboard"></Redirect>
        </Route>

        <Route
          component={() => (
            <ErrorPage
              title="Page not found"
              icon={<Icon size="5rem" color="yellow.500" name="warning" />}
            />
          )}
        />
      </Switch>
    </Router>
  );
};

export default AppRouter;
