import React from 'react';
import {Flex} from '@chakra-ui/core';

export default ({title, children, ...props}) => {
  return (
    <Flex {...props}>
      <Flex direction="column" flex={1}>
        <Flex flex={1} style={{textTransform: 'uppercase'}} align="center">
          <Flex mr={4}>{title}</Flex>
          <Flex flex={1} style={{borderBottom: '1px solid #737989'}}></Flex>
        </Flex>
        <Flex direction="row" flex={4}>
          {children}
        </Flex>
      </Flex>
    </Flex>
  );
};
