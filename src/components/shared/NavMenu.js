import React from 'react';
import {useStoreState} from 'easy-peasy';

import {Box, Flex} from '@chakra-ui/core';
import {useLocation, useHistory} from 'react-router';

import {FaChartBar} from 'react-icons/fa';
import {FiBookOpen} from 'react-icons/fi';
import {TiFlowMerge} from 'react-icons/ti';
import {IoMdSpeedometer} from 'react-icons/io';
import {RiFolderUserLine} from 'react-icons/ri';

const MenuIcon = ({name, ...props}) => {
  const defaultOptions = Object.assign({
    size: '1.5rem',
    color: 'blue.300',
  });

  const iconMap = {
    dashboard: FaChartBar,
    forecast: TiFlowMerge,
    performance: IoMdSpeedometer,
    filters: RiFolderUserLine,
    reporting: FiBookOpen,
  };

  return <Box as={iconMap[name]} {...defaultOptions} {...props} />;
};

const NavMenu = () => {
  const {pathname} = useLocation();
  const history = useHistory();

  const {menuItems} = useStoreState((state) => state.application);

  return menuItems.map((item, ...props) => (
    <NavItem
      key={item}
      item={item}
      onClick={() => {
        history.push(`/${item}`);
      }}
      isCurrent={pathname.startsWith(`/${item}`)}
      {...props}
    />
  ));
};

const NavItem = ({item, onClick, isCurrent}) => {
  const iconColor = isCurrent ? 'orange.300' : 'blue.300';
  const textColor = isCurrent ? 'blue.300' : 'white';
  const style = Object.assign({
    textTransform: 'uppercase',
    cursor: 'pointer',
    borderLeft: isCurrent ? '0.25rem solid #F6AD55' : '0.25rem solid transparent',
  });

  return (
    <Flex py={6} px={4} color={textColor} onClick={onClick} style={style} align="center">
      <MenuIcon name={item} mx={4} color={iconColor} /> {item}
    </Flex>
  );
};

export default NavMenu;
