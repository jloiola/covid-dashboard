import React from 'react';
import {Flex, Box} from '@chakra-ui/core';

export default ({label, value, unit = '', ...props}) => {
  return (
    <Flex {...props} align="center" justify="center" p={4}>
      <Flex direction="column" align="center" justify="center" color="white">
        <Box style={{fontSize: '2.5rem'}}>
          {value}
          {unit}
        </Box>
        <Box style={{textTransform: 'uppercase'}}>{label}</Box>
      </Flex>
    </Flex>
  );
};
