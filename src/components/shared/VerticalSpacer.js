import React from 'react';
import {Flex} from '@chakra-ui/core';

export default ({color, ...props}) => {
  return (
    <Flex color={color} mr={4} style={{borderRight: '1px solid white'}}>
      &nbsp;
    </Flex>
  );
};
