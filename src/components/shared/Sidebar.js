import React from 'react';
import {Flex, Box} from '@chakra-ui/core';
import NavMenu from './NavMenu';

export default ({...props}) => {
  return (
    <Box h={'100%'} bg="#38393A" {...props}>
      <Flex align="center" justify="center" color="blue.300" bg="#414243" h={65}>
        <Flex style={{fontSize: '1.75rem', fontWeight: '600'}}>
          <Box color="orange.300">&mdash;</Box>&nbsp;Board
        </Flex>
      </Flex>
      <NavMenu />
    </Box>
  );
};
