import React, {useEffect} from 'react';
import {useStoreState, useStoreActions} from 'easy-peasy';

import {Flex, Box, Icon, Spinner, Link} from '@chakra-ui/core';
import {ErrorPage, Mask} from 'chakra-ui-ext';

import {ChartBox, StatBox, Sidebar, VerticalSpacer} from '../shared';
import {PieProgressChart, BarChart, DualLineColumnChart, AreaSplineChart} from '../charts';

export default () => {
  const {
    isLoaded,
    selectedUsState,
    error,
    sinceDateTime,
    summaryStats,
    splineData,
    dualLineData,
    pieProgressData,
    barChartData,
  } = useStoreState((state) => state.dashboardView);

  const {months, days, hours, minutes} = sinceDateTime;
  const {fetchData} = useStoreActions((actions) => actions.dashboardView);

  useEffect(() => {
    fetchData(selectedUsState);
  }, [fetchData, selectedUsState]);

  if (error) {
    return <ErrorPage title={error.message} icon={<Icon name="warning" size={'5rem'} />} />;
  }

  if (!isLoaded) {
    return (
      <Mask isActive={true}>
        <Spinner />
      </Mask>
    );
  }

  return (
    <Flex h={'100%'} style={{overflow: 'hidden'}}>
      <Sidebar w={'280px'} />

      <Flex direction="column" flex={1} w={'100%'}>
        <Flex p={8} h={60}>
          <Flex justify="flex-end" align="center" flex={1} color="blue.600">
            <Link mx={4}>profile</Link>
            <Link>logout</Link>
          </Flex>
        </Flex>

        <Flex
          p={8}
          bg="blue.300"
          h={60}
          justify="center"
          align="center"
          color="white"
          style={{textTransform: 'uppercase', fontWeight: 'bold'}}
        >
          {selectedUsState}
        </Flex>

        <Flex p={4} bg="blue.200" h={150}>
          <Flex flex={1} align="space-around" justify="space-around">
            <StatBox label={'months'} value={months}></StatBox>
            <StatBox label={'days'} value={days}></StatBox>
            <StatBox label={'hours'} value={hours}></StatBox>
            <StatBox label={'minutes'} value={Math.floor(minutes)}></StatBox>

            <VerticalSpacer></VerticalSpacer>

            {summaryStats.map((stat, i) => (
              <StatBox key={i} {...stat}></StatBox>
            ))}

            <VerticalSpacer></VerticalSpacer>

            <Box w={200}>
              <PieProgressChart fontColor="white" />
            </Box>
          </Flex>
        </Flex>

        <Flex direction="row" flex={1}>
          <Flex direction="column" p={8} flex={1}>
            <ChartBox title={'By Week'} direction="row" flex={6}>
              <DualLineColumnChart {...dualLineData} />
            </ChartBox>
            <ChartBox title={'by Quarter'} direction="row" flex={4}>
              {/* odd render bug with a loop, lets just brute force  */}
              <PieProgressChart value={pieProgressData[0]} />
              <PieProgressChart value={pieProgressData[1]} />
              <PieProgressChart value={pieProgressData[2]} />
              <PieProgressChart value={pieProgressData[3]} />
            </ChartBox>
          </Flex>
          <Flex direction="column" p={8} flex={1}>
            <ChartBox title={'by Weekday'} direction="row" flex={6}>
              <AreaSplineChart {...splineData} />
            </ChartBox>
            <ChartBox title={'BY month'} direction="row" flex={4}>
              <BarChart {...barChartData} />
            </ChartBox>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
};
