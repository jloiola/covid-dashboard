import React from 'react';
import {ThemeProvider, CSSReset, theme} from '@chakra-ui/core';

import AppRouter from './AppRouter';
// Really is a wrapper for routes and global page state
const Application = () => {
  return (
    <ThemeProvider theme={theme}>
      <CSSReset />
      <AppRouter />
    </ThemeProvider>
  );
};

export default Application;
