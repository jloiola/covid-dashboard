import React from 'react';

import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

export default ({color, xAxis, series, yAxis, ...props}) => {
  const options = {
    chart: {
      zoomType: 'xy',
    },
    title: null,
    subtitle: null,
    xAxis,
    tooltip: {
      shared: true,
    },
    legend: {
      layout: 'vertical',
      align: 'left',
      x: 120,
      verticalAlign: 'top',
      y: 100,
      floating: true,
      backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || 'rgba(255,255,255,0.25)', // theme
    },
    series,
  };

  return (
    <HighchartsReact
      containerProps={{style: {height: '100%', width: '100%'}}}
      highcharts={Highcharts}
      options={options}
    />
  );
};
