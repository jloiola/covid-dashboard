import React from 'react';

import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

export default ({color, xAxis, series, ...props}) => {
  const options = {
    chart: {
      type: 'column',
    },
    title: null,
    xAxis,
    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true,
        },
      },
    },
    series,
  };

  return (
    <HighchartsReact
      containerProps={{style: {height: '100%', width: '100%'}}}
      highcharts={Highcharts}
      options={options}
    />
  );
};
