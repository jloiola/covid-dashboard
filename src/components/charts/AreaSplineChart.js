import React from 'react';

import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

export default ({color, xAxis, series, ...props}) => {
  const options = {
    chart: {
      type: 'areaspline',
    },
    title: null,
    legend: {
      layout: 'vertical',
      align: 'left',
      verticalAlign: 'top',
      x: 150,
      y: 100,
      floating: true,
      borderWidth: 1,
      backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
    },
    xAxis,
    yAxis: {},
    tooltip: {
      shared: true,
      valueSuffix: ' units',
    },
    credits: {
      enabled: false,
    },
    plotOptions: {
      areaspline: {
        fillOpacity: 0.5,
      },
    },
    series,
  };

  return (
    <HighchartsReact
      containerProps={{style: {height: '100%', width: '100%'}}}
      highcharts={Highcharts}
      options={options}
    />
  );
};
