import React from 'react';

import Highcharts from 'highcharts';
import highchartsMore from 'highcharts/highcharts-more';
import solidGauge from 'highcharts/modules/solid-gauge';
import HighchartsReact from 'highcharts-react-official';

highchartsMore(Highcharts);
solidGauge(Highcharts);

export default ({
  value = Math.floor(Math.random() * 100),
  color,
  fontColor = 'black',
  backgroundColor = 'transparent',
}) => {
  const options = {
    title: null,
    chart: {
      type: 'solidgauge',
      backgroundColor,
    },

    pane: {
      startAngle: -90,
      endAngle: 270,
      background: [
        {
          color: 'transparent',
          outerRadius: '112%',
          innerRadius: '100%',
          borderWidth: 0,
        },
      ],
    },

    yAxis: {
      min: 0,
      max: 100,
      lineWidth: 0,
      tickPositions: [],
    },

    plotOptions: {
      solidgauge: {
        dataLabels: {
          enabled: true,
          verticalAlign: 'middle',
        },
      },
    },

    series: [
      {
        data: [
          {
            color,
            radius: '112%',
            innerRadius: '100%',
            y: value,
            dataLabels: {
              format: '{y}',
              borderWidth: 0,
              style: {
                fontSize: '1.5rem',
                color: fontColor,
                textOutline: fontColor,
              },
            },
          },
        ],
      },
    ],
  };

  return (
    <HighchartsReact
      containerProps={{style: {height: '100%', width: '100%'}}}
      highcharts={Highcharts}
      options={options}
    />
  );
};
