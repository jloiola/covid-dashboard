import React from 'react';

import { render } from 'react-dom';
import { StoreProvider } from 'easy-peasy';

import store from './store';
import App from './components/App';

render(
  <StoreProvider store={store}>
    <App />
  </StoreProvider>,
  document.getElementById('root')
);
