# Services

Services generally call into REST apis for data access.

## Concerns

### Do

- A service's job is to fetch & map domain models. (if we have a server, the server should map the domain model)

### Do Not

- A service's job is NOT to build view models.
