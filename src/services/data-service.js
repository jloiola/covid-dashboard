import axios from 'axios';
import {debug} from 'ulog';

// TODO: move boilerplate http hooks into HttpClient lib
// better todo use Frisbee which is axios + fetch and easier to extend
// Generically named for our demo ...
export default class DataService {
  constructor(serverUrl, withCredentials = false, timeout = 15000) {
    debug(`server: ${serverUrl}`);
    this.http = axios.create({
      baseURL: serverUrl,
      withCredentials,
      timeout,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
    });
  }

  async getUsStates() {
    return await this.http.get('/us-states');
  }

  async getUsState(state) {
    return await this.http.get(`/us-states/${state}`);
  }
}
