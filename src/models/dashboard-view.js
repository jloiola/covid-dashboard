import {thunk, action, computed} from 'easy-peasy';
import {error} from 'ulog';
import {DateTime} from 'luxon';
import groupBy from 'lodash.groupby';
import minBy from 'lodash.minby';
import sumBy from 'lodash.sumby';

const numberFormat = (value) => new Intl.NumberFormat({style: 'decimal'}).format(value);

// this model is for a view store
export default ({dataService}) => {
  return {
    data: null,

    isLoaded: false,
    isSaving: false,
    error: null,

    setError: action((state, error) => {
      state.error = error;
    }),

    selectedUsState: 'Michigan',

    originDateTime: null,
    sinceDateTime: computed((state) =>
      state.originDateTime
        ? DateTime.local()
            .diff(state.originDateTime, ['months', 'days', 'hours', 'minutes'])
            .toObject()
        : {}
    ),

    // ok its not really a countdown, imagination
    countdownDate: DateTime.local(),

    summaryStats: [],

    reset: action((state) => {
      state.isSaving = false;
      state.isLoaded = false;
      state.error = false;
    }),

    fetchData: thunk(async (actions, usState) => {
      try {
        const {data} = await dataService.getUsState(usState);
        actions.setModel(data);

        actions.calcSummaryStats();
        actions.mapDataToAreaSpline();
        actions.mapDataToDualLine();
        actions.mapDataToPieProgress();
        actions.mapDataToBarChart();
      } catch (err) {
        error(err);
        actions.setError(err);
      }
    }),

    splineData: {xAxis: null, series: []},
    calcSummaryStats: action((state) => {
      const cases = sumBy(state.data, (item) => item.cases);
      state.summaryStats.push({label: 'cases', value: numberFormat(cases)});

      const deaths = sumBy(state.data, (item) => item.deaths);
      state.summaryStats.push({label: 'deaths', value: numberFormat(deaths)});

      state.summaryStats.push({
        label: 'rate',
        value: Math.round((deaths / cases) * 100),
        unit: '%',
      });
    }),

    pieProgressData: [],
    mapDataToPieProgress: action((state) => {
      const itemsByQuarter = groupBy(state.data, (item) => `Q${item.dateTime.toFormat('q')}`);

      // lacking time, should bucket this properly
      itemsByQuarter['Q3'] = [];
      itemsByQuarter['Q4'] = [];

      Object.keys(itemsByQuarter).forEach((qtr) => {
        const items = itemsByQuarter[qtr];

        const caseTotal = sumBy(items, (item) => item.cases);
        const deathTotal = sumBy(items, (item) => item.deaths);

        state.pieProgressData.push(Math.round((deathTotal / caseTotal) * 100));
      });
    }),

    dualLineData: {xAxis: [], series: [], yAxis: []},
    mapDataToDualLine: action((state) => {
      const itemsByWeek = groupBy(state.data, (item) => item.dateTime.toFormat('W'));

      const categories = [];
      const seriesData1 = [];
      const seriesData2 = [];

      Object.keys(itemsByWeek).forEach((week) => {
        const items = itemsByWeek[week];

        const caseTotal = sumBy(items, (item) => item.cases);
        const deathTotal = sumBy(items, (item) => item.deaths);

        categories.push(week);
        seriesData1.push(caseTotal);
        seriesData2.push(deathTotal);
      });

      state.dualLineData.xAxis.push({categories});
      state.dualLineData.series.push({
        name: 'Cases',
        type: 'column',
        data: seriesData1,
      });

      state.dualLineData.series.push({
        name: 'Deaths',
        type: 'spline',
        data: seriesData2,
      });
    }),

    barChartData: {xAxis: null, series: []},
    mapDataToBarChart: action((state) => {
      const itemsByMonth = groupBy(state.data, (item) => item.dateTime.toFormat('MMM'));

      const categories = [];
      const seriesData1 = [];
      const seriesData2 = [];

      Object.keys(itemsByMonth).forEach((month) => {
        const items = itemsByMonth[month];
        // sum without lodash
        const caseTotal = sumBy(items, (item) => item.cases);
        const deathTotal = sumBy(items, (item) => item.deaths);

        categories.push(month);
        seriesData1.push(caseTotal);
        seriesData2.push(deathTotal);
      });
      state.barChartData.xAxis = {categories};
      state.barChartData.series.push({
        name: 'Cases',
        data: seriesData1,
      });
      state.barChartData.series.push({
        name: 'Deaths',
        data: seriesData2,
      });
    }),

    mapDataToAreaSpline: action((state) => {
      const itemsByWeekday = groupBy(state.data, (item) => item.dateTime.toFormat('ccc'));

      const categories = [];
      const seriesData = [];

      Object.keys(itemsByWeekday).forEach((weekday) => {
        const items = itemsByWeekday[weekday];
        // sum without lodash
        const deathTotal = items.reduce((sum, {deaths}) => {
          sum += deaths;
          return sum;
        }, 0);
        categories.push(weekday);
        seriesData.push(deathTotal);
      });

      state.splineData.xAxis = {categories};
      state.splineData.series.push({
        name: 'Deaths',
        data: seriesData,
      });
    }),

    setModel: action((state, data) => {
      const {date} = minBy(data, (item) => item.date);
      state.originDateTime = DateTime.fromISO(date);
      state.data = data.map((item) => ({
        ...item,
        dateTime: DateTime.fromISO(item.date),
      }));
      state.isLoaded = true;
    }),
  };
};
