import {action} from 'easy-peasy';

// usually do app level user/nav state management here
// and app level load masks or error
export default () => {
  // is static/known
  const menuItems = ['dashboard', 'forecast', 'performance', 'filters', 'reporting'];
  return {
    currentUser: {
      firstName: 'Demo user',
    },
    menuItems: menuItems,
    currentView: menuItems[0],
    setCurrentView: action((state, view) => {
      state.currentView = view;
    }),
  };
};
