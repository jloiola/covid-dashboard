# View Models

View models consists of state to store data and actions to manipulate the state.

## Design

A model can be reused as a store within your favorite state manager. Here we use easy-peasy.
Each model exports a function that allows us to inject its dependencies. This is for testability.
In lieu of an elaborate DI, we do the bare minimum via a function call.

## Concerns

### Do

- A view model's job is to pair UI and domain state.
- A view model is consumed by one or many stores.

### Do Not

- A view model's job is NOT to build domain models.

## FAQ

**Q: Why is the dir just 'models' and not view-models?**

> **A:** Mostly for brevity.
>
> It represents the union of the UI state and domain objects.
